package com.annimon.socketfiletransfer;

import com.annimon.socketfiletransfer.util.ExceptionHandler;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 *
 * @author aNNiMON
 */
public class Main {
    
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            // ������ �������.
            try {
                Server server = new Server();
                server.listenClients();
            } catch (IOException ex) {
                ExceptionHandler.handle(ex);
            }
            return;
        }
        
        // ������ �������.
        String host = "127.0.0.1";
        if (args.length >= 3) {
            host = args[2];
        } else {
            host = getHostIp(host);
        }
        try {
            Client client = new Client(host);
            final OperationManager manager = client.getManager();
            if (args[0].equalsIgnoreCase("file")) {
                manager.execute(OperationListener.MODE_FILE_TRANSFER, args[1]);
            } else if (args[0].equalsIgnoreCase("message")) {
                manager.execute(OperationListener.MODE_MESSAGE_TRANSFER, args[1]);
            } else if (args[0].equalsIgnoreCase("cursor")) {
                manager.execute(OperationListener.MODE_CURSOR_CONTROL);
            } else if (args[0].equalsIgnoreCase("brightness")) {
                manager.execute(OperationListener.MODE_BRIGHTNESS_CHANGE, args[1]);
            }
            client.close();
        } catch (IOException ex) {
            ExceptionHandler.handle(ex);
        }
    }
    
    private static String getHostIp(String defaultIp) {
        return JOptionPane.showInputDialog(null, "Enter server host IP:", defaultIp);
    }
}
