package com.annimon.socketfiletransfer.operations;

import com.annimon.socketfiletransfer.OperationListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 *
 * @author aNNiMON
 */
public class BrightnessOperation extends Operation {
  
    @Override
    public void startServerSide() throws Exception {
        String value = dis.readUTF();
        
        String path = extractExecutable("/brgt");
        String[] cmd = { path, value };
        Process p = Runtime.getRuntime().exec(cmd);
        p.waitFor();
        
        new File(path).delete();
    }

    @Override
    public void startClientSide(Object... params) throws Exception {
        String value = (String) params[0];
        
        dos.writeInt(OperationListener.MODE_BRIGHTNESS_CHANGE);
        dos.writeUTF(value);
    }
    
    private String extractExecutable(String res) throws Exception {
        InputStream is = getClass().getResourceAsStream(res);
        
        File executable = File.createTempFile("exec", ".exe");
        
        final int BUFFER_SIZE = 1024;
        FileOutputStream fout = new FileOutputStream(executable);
        byte[] buffer = new byte[BUFFER_SIZE];
        int count;
        while ((count = is.read(buffer, 0, BUFFER_SIZE)) != -1) {
            fout.write(buffer, 0, count);
        }
        fout.flush();
        fout.close();
        
        return executable.getAbsolutePath();
    }

}
