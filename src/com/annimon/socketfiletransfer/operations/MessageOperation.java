package com.annimon.socketfiletransfer.operations;

import com.annimon.socketfiletransfer.OperationListener;
import com.annimon.socketfiletransfer.util.MessageHistory;
import java.io.IOException;

/**
 *
 * @author aNNiMON
 */
public class MessageOperation extends Operation {

    @Override
    public void startServerSide() throws IOException {
        String text = dis.readUTF();
        MessageHistory.addMessage(text);
        System.out.println(text);
    }

    @Override
    public void startClientSide(Object... params) throws Exception {
        String message = (String) params[0];
        
        dos.writeInt(OperationListener.MODE_MESSAGE_TRANSFER);
        dos.writeUTF(message);
    }
    
}
