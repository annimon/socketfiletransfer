package com.annimon.socketfiletransfer.operations;

import com.annimon.socketfiletransfer.OperationListener;
import com.annimon.socketfiletransfer.util.ExceptionHandler;
import com.annimon.socketfiletransfer.util.RobotUtils;
import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * ���������� ��������.
 * @author aNNiMON
 */
public class CursorOperation extends Operation {
    
    private static final int
            NONE = -1,
            TYPE_MOVE = 1,
            TYPE_CLICK = 2,
            TYPE_DRAG = 3,
            TYPE_RELEASED = 4,
            TYPE_KEY_RELEASED = 5,
            TYPE_MOVE_RELATIVE = 6,
            TYPE_MOUSE_PRESSED = 7,
            TYPE_MOUSE_RELEASED = 8,
            STOP = 10;
    
    private RobotUtils robot;
    private boolean running;
    
    public CursorOperation() {
        try {
            robot = new RobotUtils();
        } catch (AWTException ex) {
            ExceptionHandler.handle(ex);
        }
    }

    @Override
    public void startServerSide() throws IOException {
        // ��������� ������� ������, ����� ���������
        // ���������� ������ ��� ������������ ����������.
        int width = dis.readInt();
        int height = dis.readInt();
        
        if (width == height && width == -100) {
            androidTouchPadMode();
            return;
        }
        // ������ ����������.
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double dx = screenSize.getWidth() / (double) width;
        double dy = screenSize.getHeight() / (double) height;
        
        int type = NONE;
        do {
            type = dis.readInt();
            if (type == TYPE_MOVE) {
                int x = (int) (dis.readInt() * dx);
                int y = (int) (dis.readInt() * dy);
                robot.moveCursor(x, y);
            } else if (type == TYPE_CLICK) {
                int button = dis.readInt();
                robot.clickPoint(robot.getCursorPosition(), button);
            } else if (type == TYPE_KEY_RELEASED) {
                int key = dis.readInt();
                robot.pressKey(key);
            } else if (type == TYPE_DRAG) {
                robot.getRobot().mousePress(InputEvent.BUTTON1_MASK);
                int x = (int) (dis.readInt() * dx);
                int y = (int) (dis.readInt() * dy);
                robot.moveCursor(x, y);
            } else if (type == TYPE_RELEASED) {
                robot.getRobot().mouseRelease(InputEvent.BUTTON1_MASK);
            }
        } while (type != STOP);
    }
    
    private void androidTouchPadMode() throws IOException {
        int type = NONE;
        do {
            type = dis.readInt();
            if (type == TYPE_CLICK) {
                int button = dis.readInt();
                robot.clickPoint(robot.getCursorPosition(), button);
            } else if (type == TYPE_KEY_RELEASED) {
                int key = dis.readInt();
                robot.pressKey(key);
            } else if (type == TYPE_MOVE_RELATIVE) {
                int x = (int) (dis.readInt());
                int y = (int) (dis.readInt());
                robot.moveCursorRelative(x, y);
            } else if (type == TYPE_MOUSE_PRESSED) {
                int button = dis.readInt();
                robot.getRobot().mousePress(button);
            } else if (type == TYPE_MOUSE_RELEASED) {
                int button = dis.readInt();
                robot.getRobot().mouseRelease(button);
            } else if (type == TYPE_RELEASED) {
                robot.getRobot().mouseRelease(InputEvent.BUTTON1_MASK);
            }
        } while (type != STOP);
    }

    @Override
    public void startClientSide(Object... params) throws Exception {
        // �������� ������� ��� ������������� �������.
        JPanel panel = new JPanel();
        panel.setFocusable(true);
        panel.requestFocusInWindow();
        panel.setPreferredSize(Toolkit.getDefaultToolkit().getScreenSize());
        
        JDialog dialog = new JDialog();
        dialog.add(panel);
        dialog.setUndecorated(true);
        dialog.pack();
        dialog.setVisible(true);
        
        Point cursor = robot.getCursorPosition();
        System.out.println("Start tracking cursor. Now: " + cursor.toString());
        dos.writeInt(OperationListener.MODE_CURSOR_CONTROL);
        running = true;
        
        // ���������� ������ ������.
        dos.writeInt(panel.getPreferredSize().width);
        dos.writeInt(panel.getPreferredSize().height);
        
        MouseTracker tracker = new MouseTracker();
        KeyTracker keyTracker = new KeyTracker();
        panel.addMouseListener(tracker);
        panel.addMouseMotionListener(tracker);
        panel.addKeyListener(keyTracker);
        while (running) {
            Thread.sleep(10);
        }
        dialog.setVisible(false);
        System.out.println("Operation stopped");
    }

    private class KeyTracker extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            if (!running) return;
            
            try {
                dos.writeInt(TYPE_KEY_RELEASED);
                dos.writeInt(e.getKeyCode());
            } catch (IOException ex) { }
            
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                running = false;
            }
        }

    }
    
    private class MouseTracker extends MouseAdapter {
        
        @Override
        public void mouseMoved(MouseEvent e) {
            if (!running) return;
            
            int x = e.getX();
            int y = e.getY();
            
            try {
                dos.writeInt(TYPE_MOVE);
                dos.writeInt(x);
                dos.writeInt(y);
            } catch (IOException ex) { }
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            if (!running) return;
            
            int x = e.getX();
            int y = e.getY();
            
            try {
                dos.writeInt(TYPE_DRAG);
                dos.writeInt(x);
                dos.writeInt(y);
            } catch (IOException ex) { }
        }
        
        @Override
        public void mouseReleased(MouseEvent e) {
            if (!running) return;
            try {
                dos.writeInt(TYPE_RELEASED);
            } catch (IOException ex) {  }
        }
        
        @Override
        public void mouseClicked(MouseEvent e) {
            if (!running) return;
            try {
                dos.writeInt(TYPE_CLICK);
                dos.writeInt(convertMouseButtonToMask(e.getButton()));
            } catch (IOException ex) {  }
        }

        private int convertMouseButtonToMask(int button) {
            if (button == MouseEvent.BUTTON1) return MouseEvent.BUTTON1_MASK;
            if (button == MouseEvent.BUTTON2) return MouseEvent.BUTTON2_MASK;
            if (button == MouseEvent.BUTTON3) return MouseEvent.BUTTON3_MASK;
            return button;
        }
     }
    
}
