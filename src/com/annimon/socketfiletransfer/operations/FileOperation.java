package com.annimon.socketfiletransfer.operations;

import com.annimon.socketfiletransfer.OperationListener;
import com.annimon.socketfiletransfer.util.Config;
import com.annimon.socketfiletransfer.util.ExceptionHandler;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author aNNiMON
 */
public class FileOperation extends Operation {
    
    private static final int BUFFER_SIZE = 1024;
   
    @Override
    public void startServerSide() {
        FileOutputStream fout = null;
        try {
            String name = dis.readUTF();
            System.out.println("Filename: " + name);
            fout = new FileOutputStream(Config.getTransferDir() + name);
            byte[] buffer = new byte[BUFFER_SIZE];
            int count;
            while ((count = dis.read(buffer, 0, BUFFER_SIZE)) != -1) {
                fout.write(buffer, 0, count);
            }
            fout.flush();
        } catch (IOException ex) {
            ExceptionHandler.handle(ex);
        } finally {
            try {
                if (fout != null) fout.close();
            } catch (IOException ex) {
                ExceptionHandler.handle(ex);
            }
        }
    }

    @Override
    public void startClientSide(Object... params) throws Exception {
        String filePath = (String) params[0];
        File file = new File(filePath);
        
        dos.writeInt(OperationListener.MODE_FILE_TRANSFER);
        
        String name = file.getName();
        dos.writeUTF(name);
        
        FileInputStream fis = new FileInputStream(file);
        byte[] buffer = new byte[BUFFER_SIZE];
        int count;
        while ((count = fis.read(buffer, 0, BUFFER_SIZE)) != -1) {
            dos.write(buffer, 0, count);
        }
        dos.flush();
        fis.close();
    }
    
}
