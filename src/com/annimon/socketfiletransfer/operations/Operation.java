package com.annimon.socketfiletransfer.operations;

import java.io.DataInputStream;
import java.io.DataOutputStream;

/**
 *
 * @author aNNiMON
 */
public abstract class Operation {
    
    protected DataInputStream dis;
    protected DataOutputStream dos;

    public void setDataInputStream(DataInputStream dis) {
        this.dis = dis;
    }

    public void setDataOutputStream(DataOutputStream dos) {
        this.dos = dos;
    }
    
    public abstract void startServerSide() throws Exception;
    
    public abstract void startClientSide(Object... params) throws Exception;

}
