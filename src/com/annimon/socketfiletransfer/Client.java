package com.annimon.socketfiletransfer;

import com.annimon.socketfiletransfer.util.Config;
import com.annimon.socketfiletransfer.util.ExceptionHandler;
import java.io.IOException;
import java.net.Socket;

/**
 * 
 * @author aNNiMON
 */
public class Client {
    
    private final Socket clientSocket;
    private final OperationManager manager;

    public Client(String host) throws IOException {
        clientSocket = new Socket(host, Config.getPort());
        manager = new OperationManager();
        manager.setSocket(clientSocket);
    }
    
    public OperationManager getManager() {
        return manager;
    }

    public void close() {
        if (manager != null) {
            manager.close();
        }
        if (clientSocket != null) {
            try {
                clientSocket.close();
            } catch (IOException ex) {
                ExceptionHandler.handle(ex);
            }
        }
    }
    
}
