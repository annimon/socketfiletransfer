package com.annimon.socketfiletransfer;

import com.annimon.socketfiletransfer.util.ExceptionHandler;
import java.net.Socket;

/**
 * 
 * @author aNNiMON
 */
public class TransferServer implements Runnable {
    
    private final OperationListener listener;

    public TransferServer(Socket client) {
        listener = new OperationListener();
        listener.setSocket(client);
        printClientInfo(client);
    }

    @Override
    public void run() {
        try {
            listener.listenOperation();
        } catch (Exception ex) {
            ExceptionHandler.handle(ex);
        }
        listener.close();
    }
    
    private void printClientInfo(Socket client) {
        System.out.println( client.getRemoteSocketAddress().toString() );
    }

}
