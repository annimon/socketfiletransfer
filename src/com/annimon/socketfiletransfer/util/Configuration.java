package com.annimon.socketfiletransfer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * ����� ������������.
 * @author aNNiMON
 */
class Configuration {
    
    private static final String PROPERTY_RESOURCE = "config.ini";
    private static Configuration instance;
    
    static synchronized Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }
    
    
    private Properties properties;
    
    private Configuration() {
        properties = new Properties();
        readProperties();
    }
    
    public String getProperty(String key) {
        return properties.getProperty(key);
    }
    
    private void readProperties() {
        // ���� ����� � ����������� ��� ����� config.ini, �� ������ �� ��������.
        InputStream stream = getInputStreamFromFile();
        if (stream == null) {
            stream = getClass().getResourceAsStream("/" + PROPERTY_RESOURCE);
        }
        try {
            properties.load(stream);
            stream.close();
        } catch (IOException ex) {
            ExceptionHandler.handle(ex, "read property");
        }
    }
    
    private InputStream getInputStreamFromFile() {
        File file = new File(PROPERTY_RESOURCE);
        if (file.exists()) {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException ex) {
                return null;
            }
        }
        return null;
    }
}
