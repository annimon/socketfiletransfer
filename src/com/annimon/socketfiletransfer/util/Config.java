package com.annimon.socketfiletransfer.util;

import java.io.File;

/**
 *
 * @author aNNiMON
 */
public class Config {
    
    private static final String
            PORT = "PORT",
            TRANSFER_DIR = "TRANSFER_DIR";
    
    public static int getPort() {
        String value = Configuration.getInstance().getProperty(PORT);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            ExceptionHandler.handle(ex);
            return 7119;
        }
    }
    
    public static String getTransferDir() {
        String path = Configuration.getInstance().getProperty(TRANSFER_DIR);
        path = path.trim();
        if (!path.endsWith(File.separator)) {
            path = path + File.separator;
        }
        return path;
    }
}
