package com.annimon.socketfiletransfer.util;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

/**
 * ������ � ������� Robot.
 * @author aNNiMON
 */
public class RobotUtils {
    
    private static final int CLICK_DELAY = 300;
    private final Robot robot;

    public RobotUtils() throws AWTException {
        robot = new Robot();
    }
    
    public Robot getRobot() {
        return robot;
    }
    
    /**
     * �������� � ������ �����
     * @param click ����� �� ������� ����� ��������
     * @param button ������� ������ ����
     */
    public void clickPoint(Point click, int button) {
        robot.mouseMove(click.x, click.y);
        robot.mousePress(button);
        robot.delay(CLICK_DELAY);
        robot.mouseRelease(button);
    }
    
    /**
     * ������ �������.
     * @param keyCode ��� �������
     */
    public void pressKey(int keyCode) {
        try {
            robot.keyPress(keyCode);
            robot.keyRelease(keyCode);
        } catch (Exception ex) { }
    }
    
    /**
     * �������� ������� �������.
     * @return ������� ������� �������
     */
    public Point getCursorPosition() {
        PointerInfo pointerInfo = MouseInfo.getPointerInfo();
        return pointerInfo.getLocation();
    }
    
    /**
     * ����������� ������ �� ������ ����������.
     * @param dx ���������� ��-�����������
     * @param dy ���������� ��-���������
     */
    public void moveCursor(int dx, int dy) {
        robot.mouseMove(dx, dy);
    }
    
    /**
     * ����������� ������ ������������ ������� ����������.
     * @param dx ���������� ��-�����������
     * @param dy ���������� ��-���������
     */
    public void moveCursorRelative(int dx, int dy) {
        Point cursorPoint = getCursorPosition();
        cursorPoint.translate(dx, dy);
        robot.mouseMove(cursorPoint.x, cursorPoint.y);
    }

    /**
     * �������������� ��������� ���������
     * @param text "����������" �����
     */
    public void writeMessage(String text) {
        for (char symbol : text.toCharArray()) {
            boolean needShiftPress = Character.isUpperCase(symbol) && Character.isLetter(symbol);
            if(needShiftPress) {
                robot.keyPress(KeyEvent.VK_SHIFT);
            }
            int event = KeyEvent.getExtendedKeyCodeForChar(symbol);
            try {
                robot.keyPress(event);
            } catch (Exception e) {}
            if(needShiftPress) {
                robot.keyRelease(KeyEvent.VK_SHIFT);
            }
        }
    }
    
    /*
     * ��������� �������� �������� [width x height] � ������ � ������� [x, y]
     * ���� width ��� height ����� -1, �� ���������� ���� �����.
     */
    public BufferedImage getImage(int x, int y, int width, int height) {
        Rectangle area;
        if ((width == -1) || (height == -1)) {
            area = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        } else area = new Rectangle(x, y, width, height);
        return robot.createScreenCapture(area);
    }
}
